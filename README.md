# NewProEver 画面の実装サンプルです。

## 動かすためには

[Node.js](https://nodejs.org/)をインストールします。

Angular CLIのインストール

`npm install -g @angular/cli` 

Json Serverのインストール

`npm install -g json-server`

ライブラリのインストール

`npm install`

開発サーバー実行

`ng serve`

APIモックサーバー実行（新規のターミナル／PowerShellウィンドウを立ち上げる）

`cd api-mock`

`node server.js`

APIのレスポンスファイルを変更するには、引数にファイル名を指定します。

`node server.js db.sample.json`

ブラウザで以下のURLを表示してください。

`http://localhost:4200/`

複数の環境を切り替えるには、ng serve にオプションを追加します（IT環境の例）。

`ng serve -c it`

環境を追加するには、environment.{環境名}.ts を environments フォルダに作成し、angular.json の configurations セクション、および serve セクション配下の configurations セクションを追加します。

APIの向き先はパスごとに指定可能です。environment.ts の以下の箇所を参考にしてください。

```
    api: [
        {
            path: '',
            host: 'http://localhost:3000/',
            prefix: '',
            suffix: ''
        },
        {
            path: 'search_issue',
            host: 'http://localhost:3000/',
            prefix: '',
            suffix: ''
        }
    ]
```

# ProeverMaterial

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
