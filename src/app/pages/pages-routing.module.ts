import { NgModule } from '@angular/core';
import { Router, Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { TopComponent } from './top/top.component';
import { MypageComponent } from './mypage/mypage.component';
import { DefaultRouteReuseStrategy } from '@angular/router/src/route_reuse_strategy';
import { ProjectGuard } from '../basis/guards/project.guard';
import { AuthGuard } from '../basis/guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'top',
                component: TopComponent
            },
            {
                path: 'mypage',
                component: MypageComponent
            },
            {
                path: 'biz',
                loadChildren: '../biz/biz.module#BizModule',
            },
        ],
    }, {
        path: '**',
        redirectTo: 'top'
    }
]
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
})
export class PagesRoutingModule { }
