import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared.module';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { TopComponent } from './top/top.component';
import { PartsModule } from '../parts/parts.module';
import { MypageComponent } from './mypage/mypage.component';
import { ConfirmDialogComponent } from '../basis/components/confirm-dialog/confirm-dialog.component';
import { BasisModule } from '../basis/basis.module';

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        SharedModule,
        PagesRoutingModule,
        PartsModule,
        BasisModule,
    ],
    declarations: [PagesComponent, TopComponent, MypageComponent],
    entryComponents: [ConfirmDialogComponent]
})
export class PagesModule { }
