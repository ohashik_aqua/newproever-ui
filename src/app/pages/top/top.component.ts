import { Component, OnInit } from '@angular/core';
import { ContextService } from '../../basis/services/context.service';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {

    public userId: string;

    constructor(
        private context: ContextService
    ) { }

    ngOnInit() {
        this.userId = this.context.getUserId();
    }

}
