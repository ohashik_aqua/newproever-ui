import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Language } from 'angular-l10n';
import { MatSnackBar, MatDialog } from '@angular/material';
import { ScreenBehaviorService, SheetContent } from '../basis/services/screen-behavior.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

    @Language() lang: string;

    public isNavOpened: boolean = true;

    public isSheetOpened: boolean = false;

    public sheetContent: SheetContent;

    constructor(
        private router: Router,
        private snackBar: MatSnackBar,
        private dialog: MatDialog,
        private behavior: ScreenBehaviorService
    ) { }

    ngOnInit() {
        console.log(`pages init.`);
        this.behavior.getSheet().subscribe(
            res => {
                console.log(res);
                this.isSheetOpened = res;
                this.sheetContent = this.behavior.sheetContent;
            }
        );
        this.behavior.getMessage().subscribe(
            res => {
                console.log(res);
                if (res) {
                    this.showMessage(res);
                }
            }
        );
    }

    public toggleNav() {
        this.isNavOpened = !this.isNavOpened;
    }

    public goTop() {
        this.router.navigate(['top']);
    }

    public showMessage(message: string) {
        this.snackBar.open(message, '');
    }

}
