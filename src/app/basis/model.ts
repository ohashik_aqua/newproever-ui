export interface Model {

    toJson(): object;

    fromJson(json: object): Model;

    clear(): void;

}
