import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchSettingComponent } from './components/search-setting/search-setting.component';
import { PartsModule } from '../parts/parts.module';
import { SharedModule } from '../shared.module';
import { FormsModule } from '@angular/forms';
import { DateCompareValidator } from './validators/date-compare.validator';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { SearchFieldsComponent } from './components/search-fields/search-fields.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PartsModule,
    SharedModule,
  ],
  declarations: [SearchSettingComponent, DateCompareValidator, ConfirmDialogComponent, SearchFieldsComponent],
  exports: [SearchSettingComponent, SearchFieldsComponent, DateCompareValidator]
})
export class BasisModule { }
