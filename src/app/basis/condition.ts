import { Model } from "./model";
import { HumanResource } from "../models/human-resource";

export class Condition implements Model {

    id?: number;

    name?: string;

    targetName?: string;

    projectId?: number;

    isPublic?: boolean;

    humanResourceId?: number;

    conditions?: ConditionContent[];

    columns?: ConditionColumn[];

    rows?: number;

    from?: number;

    to?: number;

    selectionValues?: SelectionValue[];

    public fromJson(json: object) {
        return this;
    }

    public toJson() {
        return {};
    }

    public clear() {
        if (!this.conditions) {
            return;
        }
        this.conditions.forEach(
            item => {
                if (item.type === 'human_resource') {
                    item.value = new HumanResource();
                } else {
                    item.value = null;
                }
            }
        );
    }

}

export interface ConditionContent {

    name: string;

    enabled: boolean;

    operator: string;

    type: string;

    value: any;
}

export interface ConditionColumn {

    name: string;

    enabled: boolean;

    sortOrder: number;

    sortType: string;

}

export interface SelectionValue {

    key: string;

    values: any[];

}