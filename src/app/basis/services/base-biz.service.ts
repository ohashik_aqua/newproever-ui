import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Condition } from '../condition';
import { Model } from '../model';
import { ApiService } from './api.service';
import { BizService } from './biz.service';
import { Attachment } from '../../models/attachment';
import { OptionItem } from '../../models/option-item';
import { WorkItem } from '../../models/work-item';
import { SearchResult } from '../search-result';

export abstract class BaseBizService<T extends Model> implements BizService<T> {

    constructor(protected api: ApiService) { }

    public abstract search(condition: Condition | Model): Observable<SearchResult<T>>;

    public abstract get(id: number): Observable<T>;

    public abstract save(model: T): Observable<T>;

    public abstract delete(model: T): Observable<T>;

    public abstract build(): Observable<T>;

    public getRelations(id: number): Observable<WorkItem[]> {
        let workItem = new WorkItem();
        workItem.id = id;
        return this.api.call<any>('get_related_items', workItem).pipe<WorkItem[]>(
            map(
                res => {
                    let results: WorkItem[] = [];
                    res.forEach(
                        item => {
                            results.push(new WorkItem().fromJson(item));
                        }
                    );
                    return results;
                }
            )
        );
    }

    public getAttachments(id: number): Observable<Attachment[]> {
        let workItem = new WorkItem();
        workItem.id = id;
        return this.api.call<any>('get_attachments', workItem).pipe<Attachment[]>(
            map(
                res => {
                    let results: Attachment[] = [];
                    res.forEach(
                        item => {
                            results.push(new Attachment().fromJson(item));
                        }
                    );
                    return results;
                }
            )
        );
    }

    public getOptionItems(id: number): Observable<OptionItem[]> {
        return null;
    }

}
