import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Condition } from '../condition';
import { Model } from '../model';
import { BizService } from './biz.service';

@Injectable({
  providedIn: 'root'
})
export class NullService implements BizService<Model> {

    constructor() { }

    public search(condition: Condition): Observable<Model[]> {
        return null;
    }

    public get(id: number): Observable<Model> {
        return null;
    }

    public save(model: Model): Observable<Model> {
        return null;
    }

    public delete(model: Model): Observable<Model> {
        return null;
    }

    public build(): Observable<Model> {
        return null;
    }

}
