import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Condition } from '../condition';
import { ApiService } from './api.service';
import { HumanResource } from '../../models/human-resource';

@Injectable({
  providedIn: 'root'
})
export class SearchSettingService {

    private conditions: Map<string, BehaviorSubject<Condition>>;

    constructor(private api: ApiService) {
        this.conditions = new Map<string, BehaviorSubject<Condition>>();
    }

    public get(id?: number): Observable<Condition> {
        return this.api.call<Condition>('get_condition', null).pipe(
            map(
                res => {
                    this.initCondition(res);
                    return res;
                }
            )
        );
    }

    public find(key: string): Observable<Condition> {
        if (this.conditions.has(key)) {
            return this.conditions.get(key).asObservable();
        }
        return this.api.call<Condition>('get_condition', null).pipe(
            map(
                res => {
                    this.conditions.set(key, new BehaviorSubject<Condition>(res));
                    this.initCondition(res);
                    return res;
                }
            )
        );
    }

    public update(key: string, condition: Condition) {
        this.conditions.set(key, new BehaviorSubject<Condition>(condition));
    }

    public search(targetName: string): Observable<Condition[]> {
        return this.api.call<Condition[]>('search_conditions', null).pipe(
            map(
                res => {
                    return res;
                }
            )
        );
    }

    private initCondition(condition: Condition) {
        condition.clear = () => {
            if (!condition.conditions) {
                return;
            }
            condition.conditions.forEach(
                item => {
                    if (item.type === 'human_resource') {
                        item.value = new HumanResource();
                    } else {
                        item.value = null;
                    }
                }
            );
        };
        condition.conditions.forEach(
            con => {
                if (con.enabled && con.type === 'human_resource' && !con.value) {
                    console.log(`human resource.`);
                    con.value = new HumanResource();
                }
            }
        );
    }

}
