import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Project } from '../../models/project';
import { environment } from '../../../environments/environment';
import { LocaleService } from 'angular-l10n';

const USER_ID_KEY = 'userId';

const LANG_KEY = 'lang';

const PROJECT_KEY = 'project';

@Injectable({
  providedIn: 'root'
})
export class ContextService {

    private selectedProject: BehaviorSubject<Project>;

    private userId: string;

    private lang: string;

    constructor(
        private locale: LocaleService
    ) {
        // ローカルストレージからユーザー情報を復元します。
        this.userId = '';
        if (!environment.production) {
            let userId = localStorage.getItem(USER_ID_KEY);
            if (userId) {
                this.userId = userId;
            }
        }
        console.log(window.navigator.language);
        let lang = localStorage.getItem(LANG_KEY);
        if (!lang) {
            lang = this.judgeLang(window.navigator.language);
            if (!lang) {
                lang = 'en';
            }
        }
        this.setLanguage(lang);
        let project = localStorage.getItem(PROJECT_KEY);
        console.log(`resume project.${project}`);
        if (project) {
            this.selectedProject = new BehaviorSubject<Project>(new Project().fromJson(JSON.parse(project)));
        } else {
            this.selectedProject = new BehaviorSubject<Project>(null);
        }
    }

    public getSelectedProject(): Observable<Project> {
        return this.selectedProject.asObservable();
    }

    public setSelectedProject(project: Project) {
        this.selectedProject.next(project);
        localStorage.setItem(PROJECT_KEY, JSON.stringify(project.toJson()));
    }

    public clearSelectedProject() {
        this.selectedProject.next(null);
        localStorage.removeItem(PROJECT_KEY);
    }

    public setUserId(userId: string) {
        this.userId = userId;
        localStorage.setItem(USER_ID_KEY, userId);
    }

    public getUserId(): string {
        return this.userId;
    }

    public setLanguage(lang: string) {
        this.locale.setCurrentLanguage(lang);
        localStorage.setItem(LANG_KEY, lang);
    }

    public getLanguage(): string {
        return this.lang;
    }

    private judgeLang(lang: string): string {
        if (!lang) {
            return null;
        }
        if (lang.startsWith('en')) {
            return 'en';
        }
        if (lang.startsWith('ja')) {
            return 'ja';
        }
        if (lang.startsWith('zh')) {
            if (lang.toLowerCase() === 'zh-tw') {
                return 'zh-tw'
            }
            return 'zh-cn';
        }
        return null;
    }

}
