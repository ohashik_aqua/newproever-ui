import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ScreenBehaviorService {

    private message: BehaviorSubject<string>;

    private sheet: BehaviorSubject<boolean>;

    private confirmer: BehaviorSubject<string>;

    private confirmResult: BehaviorSubject<boolean>;

    public sheetContent: SheetContent;

    private previousUrl: string;

    private currentUrl: string;

    constructor(
        private router: Router
    ) {
        this.init();
        this.router.events.subscribe(
            event => {
                if (event instanceof NavigationEnd) {   
                    this.previousUrl = this.currentUrl;
                    this.currentUrl = event.url;
                    console.log(`previous.${this.previousUrl} current.${this.currentUrl}`)     
                };
            }
        );
    }

    public getMessage() {
        return this.message.asObservable();
    }

    public showMessage(message: string) {
        this.message.next(message);
    }

    public getSheet(): Observable<boolean> {
        return this.sheet.asObservable();
    }

    public openSheet(sheetContent?: SheetContent) {
        this.sheetContent = sheetContent;
        this.sheet.next(true);
    }

    public closeSheet() {
        this.sheet.next(false);
        this.sheetContent = null;
    }

    public getConfirmer(): Observable<string> {
        return this.confirmer.asObservable();
    }

    public showConfirmer(message: string): Observable<boolean> {
        this.confirmer.next(message);
        return this.confirmResult.asObservable();
        // return this.confirmResult.asObservable().pipe(
        //     first()
        // );
    }

    public setConfirmResult(result: boolean) {
        this.confirmResult.next(result);
    }

    public init() {
        this.sheet = new BehaviorSubject<boolean>(false);
        this.message = new BehaviorSubject<string>(null);
        this.confirmer = new BehaviorSubject<string>(null);
        this.confirmResult = new BehaviorSubject<boolean>(false);
    }

    public getPreviousUrl(): string {
        return this.previousUrl;
    }

    public getCurrentUrl(): string {
        return this.currentUrl;
    }

}

export interface SheetContent {

    id: number;

    title: string;

    subTitle?: string;

    items: SheetItem[];

}

export interface SheetItem {

    name: string;

    action: Function;

    icon?: string;

}

export interface ConfirmContent {

    message: string;

    result: boolean;

}
