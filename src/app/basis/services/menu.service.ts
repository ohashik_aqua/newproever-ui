import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Menu } from '../../models/menu';
import { ApiService } from './api.service';
import { ContextService } from './context.service';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

    private projectId: number;

    private menus: BehaviorSubject<Menu[]>;

    constructor(
        private api: ApiService,
        private context: ContextService
    ) {
        this.menus = new BehaviorSubject<Menu[]>([]);
        this.context.getSelectedProject().subscribe(
            res => {
                this.load();
            }
        );
    }

    private load() {
        this.api.call<any>('get_menus').subscribe(
            res => {
                let results: Menu[] = [];
                res.forEach(
                    item => {
                        results.push(new Menu().fromJson(item));
                    }
                );
                this.menus.next(results);
            }
        );
    }

    public getMenus(): Observable<Menu[]> {
        return this.menus.asObservable();
    }
}
