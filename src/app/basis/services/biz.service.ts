
import { Model } from '../model';
import { Condition } from '../condition';
import { Observable } from 'rxjs';
import { WorkItem } from '../../models/work-item';
import { Attachment } from '../../models/attachment';
import { OptionItem } from '../../models/option-item';
import { SearchResult } from '../search-result';

export interface BizService<T extends Model> {

    search(condition: Condition | Model): Observable<T[]> | Observable<SearchResult<T>>;

    get(id: number): Observable<T>;

    save(model: T): Observable<T>;

    delete(model: T): Observable<T>;

    build(): Observable<T>;

    // getRelations(id: number): Observable<WorkItem[]>;

    // getAttachments(id: number): Observable<Attachment[]>;

    // getOptionItems(id: number): Observable<OptionItem[]>;

}
