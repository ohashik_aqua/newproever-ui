import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Model } from '../model';
import { ApiResponse } from '../api-response';
import { ContextService } from './context.service';
import { Location } from '@angular/common';
import { isNumber } from 'util';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    private defaultSetting: ApiSetting;

    private projectId = 0;

    constructor(
        private httpClient: HttpClient,
        private location: Location,
        private context: ContextService
    ) {
        this.defaultSetting = environment.api.find(setting => !setting.path);
        this.context.getSelectedProject().subscribe(
            res => {
                if (res) {
                    this.projectId = res.id;
                } else {
                    this.projectId = 0;
                }
            }
        );
    }

    private findApiSetting(path: string): ApiSetting {
        let setting = environment.api.find(item => item.path === path);
        if (setting) {
            return setting;
        }
        return this.defaultSetting;
    }

    /**
     * APIを呼び出し、処理結果をApiResponse型で返却します。
     * @param path APIの呼び出し先。
     * @param payload APIのリクエストデータ。
     */
    public callAsApiResponse<T>(path: string, payload?: Model | Model[]): Observable<ApiResponse<T>> {
        let setting = this.findApiSetting(path);
        let url = setting.host + setting.prefix + path + setting.suffix;
        let body: any = null;
        console.log(typeof(payload));
        if (payload) {
            if (Array.isArray(payload)) {
                body = [];
                (payload as Model[]).forEach(
                    model => {
                        body.push(model.toJson());
                    }
                );
            } else {
                if (payload.toJson) {
                    body = payload.toJson();
                } else {
                    body = payload;
                }
            }
        } else {
            body = {};
        }
        if (this.projectId) {
            body.project_id = this.projectId;
        }
        console.log(this.location.path());
        let currentPath = this.getCurrentPath(this.location.path());
        console.log(currentPath);
        body.screen_id = currentPath;
        let options = {
            headers: new HttpHeaders({
                'content-type': 'application/json'
            }),
            withCredentials: true
        };
        console.log(`call api. ${url} ${body}`);
        return this.httpClient.post<{response: ApiResponse<T>}>(url, {request: body}, options).pipe(
            map(
                res => {
                    return res.response;
                }
            )
        );
    }

    /**
     * APIを呼び出し、処理結果をModelまたはModelの配列で返却します。
     * @param path APIの呼び出し先。
     * @param payload APIのリクエストデータ。
     */
    public call<T>(path: string, payload?: Model | Model[]): Observable<T> {
        return this.callAsApiResponse<T>(path, payload).pipe(
            map(
                res => {
                    return res.content;
                }
            )
        );
    }

    public callWithFiles<T>(path: string, payload: Model | Model[], files: File[]): Observable<T> {
        let setting = this.findApiSetting(path);
        let url = setting.host + setting.prefix + path + setting.suffix;
        // let body: any = null;
        // if (payload) {
        //     body = payload;
        // }
        let body = new FormData();
        files.forEach(
            file => body.append('file', file)
        );
        let options = {
            // headers: new HttpHeaders({
            //     'content-type': 'application/json'
            // }),
            withCredentials: true
        };
        body.append('data', new Blob([JSON.stringify(payload)], {type: 'application/json'}));
        console.log(`call api. ${url} ${payload}`);
        return this.httpClient.post<{response: ApiResponse<T>}>(url, {request: body}, options).pipe(
            map(
                res => {
                    console.log(res);
                    return res.response.content;
                }
            )
        );
    }

    private getCurrentPath(path: string): string {
        if (!path) {
            return '';
        }
        let tmpPath = path.substring(0, path.indexOf('?') > 0 ? path.indexOf('?') : path.length);
        let paths = tmpPath.split('/');
        if (paths.length === 1) {
            return tmpPath;
        }
        let suffix = paths[paths.length - 1];
        if (!isNaN(Number(suffix))) {
            return tmpPath.substring(0, path.length - suffix.length - 1);
        }
        return tmpPath;
    }
}

function create<T>(ctor: { new(): T }) {
    return new ctor();
}

export interface ApiSetting {

    path: string;

    host: string;

    prefix: string;

    suffix: string;
}
