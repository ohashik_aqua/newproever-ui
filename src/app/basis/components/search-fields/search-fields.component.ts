import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { MatDialog } from '@angular/material';
import { ScreenBehaviorService } from '../../services/screen-behavior.service';
import { Condition, ConditionContent, SelectionValue } from '../../condition';
import { HumanResourceService } from '../../../biz/human-resource/human-resource.service';
import { HumanResource } from '../../../models/human-resource';
import { Observable } from 'rxjs';
import { HumanResourceDialogComponent } from '../../../biz/human-resource/human-resource-dialog/human-resource-dialog.component';

@Component({
  selector: 'app-search-fields',
  templateUrl: './search-fields.component.html',
  styleUrls: ['./search-fields.component.scss']
})
export class SearchFieldsComponent extends BaseComponent {

    @Input() condition: Condition;

    @Input() selectionValues: SelectionValue[];

    public humanResources: HumanResource[];

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        private humanResourceService: HumanResourceService
    ) {
        super(router, route, translation, dialog, behavior);
    }

    ngOnInit() {
        // this.condition.conditions.forEach(
        //     con => {
        //         if (con.enabled && con.type === 'human_resource' && !con.value) {
        //             console.log(`human resource.`);
        //             con.value = new HumanResource();
        //         }
        //     }
        // );
    }

    public searchHumanResources() {
        this.humanResourceService.search(null).subscribe(
            res => {
                console.log(res);
                this.humanResources = res.results;
            }
        )
    }

    public selectHumanResource(conditionContent: ConditionContent) {
        this.humanResources = [];
        let dialogRef = this.dialog.open(HumanResourceDialogComponent, {width: '100vw', height: '100vh', maxWidth:'100vw'});
        return dialogRef.afterClosed().subscribe(
            res => {
                if (res) {
                    conditionContent.value = res;
                }
            }
        );
    }

    public getValues(key: string) {
        if (!this.selectionValues) {
            return [];
        }
        let index = this.selectionValues.findIndex(
            selectionValue => selectionValue.key === key
        );
        if (index < 0) {
            return [];
        }
        return this.selectionValues[index].values;
    }

    public hasValues(key: string): boolean {
        return this.selectionValues && Boolean(this.selectionValues.find(
            selectionValue => selectionValue.key === key
        ));
    }

}

