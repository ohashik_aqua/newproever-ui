import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatDialog } from '@angular/material';
import { Condition, ConditionContent, ConditionColumn, SelectionValue } from '../../condition';
import { SearchSettingService } from '../../services/search-setting.service';
import { BaseComponent } from '../base.component';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { ScreenBehaviorService } from '../../services/screen-behavior.service';
import { HumanResourceService } from '../../../biz/human-resource/human-resource.service';
import { HumanResource } from '../../../models/human-resource';
import { HumanResourceCondition } from '../../../models/human-resource-condition';
import { HumanResourceDialogComponent } from '../../../biz/human-resource/human-resource-dialog/human-resource-dialog.component';

@Component({
  selector: 'app-search-setting',
  templateUrl: './search-setting.component.html',
  styleUrls: ['./search-setting.component.scss']
})
export class SearchSettingComponent extends BaseComponent {

    public conditionColumns = ['condition.name', 'condition.value', 'condition.enabled'];

    public columnColumns = ['column.name', 'column.sort_order', 'column.sort_type', 'column.enabled'];

    public dataSourceCondition = new MatTableDataSource<ConditionContent>();

    public dataSourceColumns = new MatTableDataSource<ConditionColumn>();

    public condition: Condition;

    public humanResources: HumanResource[];

    private selectionValues: SelectionValue[];

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        public dialogRef: MatDialogRef<SearchSettingComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Condition,
        private searchSetting: SearchSettingService,
        private humanResourceService: HumanResourceService
    ) {
        super(router, route, translation, dialog, behavior);
        if (this.data.selectionValues) {
            this.selectionValues = this.data.selectionValues;
        }
    }

    ngOnInit() {
        if (this.data.selectionValues) {
            this.selectionValues = this.data.selectionValues;
        }
        this.searchSetting.get(this.data.id ? this.data.id : 0).subscribe(
            res => {
                this.condition = res;
                this.dataSourceCondition.data = this.condition.conditions;
                this.dataSourceColumns.data = this.condition.columns;
                this.condition.conditions.forEach(
                    item => {
                        if (item.type === 'human_resource' && !item.value) {
                            item.value = new HumanResource();
                        }
                    }
                );
            }
        );
        this.humanResourceService.search(new HumanResourceCondition()).subscribe(
            res => {
                this.humanResources = res.results;
            }
        );
    }

    public close() {
        this.dialogRef.close();
    }

    public save() {
        console.log(this.condition);
        this.searchSetting.update(this.condition.targetName, this.condition);
        this.dialogRef.close();
    }

    public searchHumanResources() {
        this.humanResourceService.search(null).subscribe(
            res => {
                console.log(res);
                this.humanResources = res.results;
            }
        )
    }

    public selectHumanResource(conditionContent: ConditionContent) {
        this.humanResources = [];
        let dialogRef = this.dialog.open(HumanResourceDialogComponent, {width: '100vw', height: '100vh', maxWidth:'100vw'});
        return dialogRef.afterClosed().subscribe(
            res => {
                if (res) {
                    conditionContent.value = res;
                }
            }
        );
    }

    public getValues(key: string) {
        if (!this.selectionValues) {
            return [];
        }
        let index = this.selectionValues.findIndex(
            selectionValue => selectionValue.key === key
        );
        if (index < 0) {
            return [];
        }
        return this.selectionValues[index].values;
    }

    public hasValues(key: string): boolean {
        return this.selectionValues && Boolean(this.selectionValues.find(
            selectionValue => selectionValue.key === key
        ));
    }

    public enableSort(column: ConditionColumn): boolean {
        return this.condition.columns.filter(
            value => {
                return value.name !== column.name && Boolean(value.sortOrder);
            }
        ).length < 2;
    }

    public changeSort(column: ConditionColumn) {
        if (column.sortOrder) {
            return;
        }
        column.sortType = '';
    }

}
