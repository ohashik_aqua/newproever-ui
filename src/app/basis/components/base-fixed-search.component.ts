import { SelectionModel } from '@angular/cdk/collections';
import { ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource, PageEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { map, startWith, switchMap } from '../../../../node_modules/rxjs/operators';
import { environment } from '../../../environments/environment';
import { WorkItem } from '../../models/work-item';
import { ConditionModel } from '../condition-model';
import { Model } from '../model';
import { BaseBizService } from '../services/base-biz.service';
import { ScreenBehaviorService, SheetContent, SheetItem } from '../services/screen-behavior.service';
import { SearchSettingService } from '../services/search-setting.service';
import { BaseComponent } from './base.component';

export abstract class BaseFixedSearchComponent<T extends Model, TCondition extends ConditionModel> extends BaseComponent {
    
    protected DISPLAYED_COLUMNS = [];

    public displayedColumns = [];

    public condition: TCondition;

    public searchOpened = true;

    public dataSource = new MatTableDataSource<T>();

    public selection = new SelectionModel<T>(true, []);

    public pageEvent: PageEvent;

    public totalCount = 0;

    @ViewChild(MatPaginator) paginator: MatPaginator;

    @ViewChild(MatSort) sort: MatSort;

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        protected searchSetting: SearchSettingService,
        protected service: BaseBizService<T>
    ) {
        super(router, route, translation, dialog, behavior);
    }

    protected initDataSource() {
        this.paginator.pageIndex = (this.route.snapshot.queryParams['page']) ? +(this.route.snapshot.queryParams['page']) : 0;
        this.paginator.pageSize = (this.route.snapshot.queryParams['size']) ? +(this.route.snapshot.queryParams['size']) : environment.defaultPageSize;
        for (let key in this.route.snapshot.queryParams) {
            if (this.condition[key] !== undefined) {
                this.condition[key] = this.route.snapshot.queryParams[key]
            }
        }
        this.dataSource.sort = this.sort;
        this.handlePageEvent();
    }

    protected handlePageEvent() {
        this.paginator.page.pipe(
            startWith({}),
            switchMap(
                () => {
                    let from = this.paginator.pageSize * this.paginator.pageIndex;
                    let to = from + this.paginator.pageSize;
                    console.log(`page: ${this.paginator.pageIndex} size: ${this.paginator.pageSize} from:${from} to:${to}`);
                    this.condition.from = from;
                    this.condition.to = to;
                    return this.service.search(this.condition);
                }
            ),
            map(
                res => {
                    this.totalCount = res.total;
                    return res.results;
                }
            )
        ).subscribe(
            res => {
                if (this.totalCount === 0) {
                    this.showMessage('message.common.search.no_result');
                }
                this.dataSource.data = res;
                const urlTree = this.router.createUrlTree([], {
                    queryParams: { page: this.paginator.pageIndex, size: this.paginator.pageSize },
                    queryParamsHandling: 'merge',
                    preserveFragment: true });
                this.router.navigateByUrl(urlTree); 
            }
        );
    }

    public isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
      }

    public masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    public search() {
        this.service.search(this.condition).subscribe(
            res => {
                console.log(res);
                this.dataSource.data = res.results;
                this.totalCount = res.total;
                if (this.totalCount === 0) {
                    this.showMessage('message.common.search.no_result');
                }
            }
        );
    }

    public openSearch() {
        this.searchOpened = !this.searchOpened;
    }

    public changeModel(key: string) {
        if (this.condition[key] === ' undefined') {
            return;
        }
        let params = {};
        params[key] = this.condition[key];
        const urlTree = this.router.createUrlTree([], {
            queryParams: params,
            queryParamsHandling: 'merge',
            preserveFragment: true });
        this.router.navigateByUrl(urlTree); 
    }

    public clear() {
        this.condition.clear();
        let params = {};
        for (let key in this.route.snapshot.queryParams) {
            if (this.condition[key] !== undefined) {
                params[key] = null;
            } else {
                params[key] = this.route.snapshot.queryParams[key];
            }
        }
        const urlTree = this.router.createUrlTree([], {
            queryParams: params,
            queryParamsHandling: 'merge' });
        this.router.navigateByUrl(urlTree); 
    }

}
