import { SelectionModel } from '@angular/cdk/collections';
import { ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource, PageEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { Attachment } from '../../models/attachment';
import { WorkItem } from '../../models/work-item';
import { Condition, SelectionValue } from '../condition';
import { Model } from '../model';
import { BaseBizService } from '../services/base-biz.service';
import { ScreenBehaviorService, SheetContent, SheetItem } from '../services/screen-behavior.service';
import { SearchSettingService } from '../services/search-setting.service';
import { BaseComponent } from './base.component';
import { SearchSettingComponent } from './search-setting/search-setting.component';
import { startWith, map, switchMap } from '../../../../node_modules/rxjs/operators';
import { environment } from '../../../environments/environment';

export abstract class BaseSearchComponent<T extends Model> extends BaseComponent {

    protected DISPLAYED_COLUMNS = [];

    public displayedColumns = [];

    public condition: Condition;

    public searchOpened = true;

    public dataSource = new MatTableDataSource<T>();

    public selection = new SelectionModel<T>(true, []);

    public pageEvent: PageEvent;

    protected targetName: string;

    public totalCount = 0;

    public conditions: Condition[];

    protected selectionValues: SelectionValue[];

    public selectedConditionId: number;

    @ViewChild(MatPaginator) paginator: MatPaginator;

    @ViewChild(MatSort) sort: MatSort;

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        protected searchSetting: SearchSettingService,
        protected service: BaseBizService<T>
    ) {
        super(router, route, translation, dialog, behavior);
        this.selectionValues = [];
    }

    protected initDataSource() {
        this.paginator.pageIndex = (this.route.snapshot.queryParams['page']) ? +(this.route.snapshot.queryParams['page']) : 0;
        this.paginator.pageSize = (this.route.snapshot.queryParams['size']) ? +(this.route.snapshot.queryParams['size']) : environment.defaultPageSize;
        this.dataSource.sort = this.sort;
        this.searchSetting.search(this.targetName).subscribe(
            res => {
                this.conditions = res;
            }
        );
        this.searchSetting.find(this.targetName).subscribe(
            res => {
                this.condition = res;
                this.selectedConditionId = res.id;
                console.log(this.condition);
                this.setDisplayedColumns();
                this.handlePageEvent();
            }
        );
    }

    protected handlePageEvent() {
        this.paginator.page.pipe(
            startWith({}),
            switchMap(
                () => {
                    let from = this.paginator.pageSize * this.paginator.pageIndex;
                    let to = from + this.paginator.pageSize + 1;
                    console.log(`page: ${this.paginator.pageIndex} size: ${this.paginator.pageSize} from:${from} to:${to}`);
                    this.condition.from = from;
                    this.condition.to = to;
                    return this.service.search(this.condition);
                }
            ),
            map(
                res => {
                    this.totalCount = res.total;
                    return res.results;
                }
            )
        ).subscribe(
            res => {
                if (this.totalCount === 0) {
                    this.showMessage('message.common.search.no_result');
                }
                this.dataSource.data = res;
                const urlTree = this.router.createUrlTree([], {
                    queryParams: { page: this.paginator.pageIndex, size: this.paginator.pageSize },
                    queryParamsHandling: 'merge',
                    preserveFragment: true });
                this.router.navigateByUrl(urlTree); 
            }
        );
    }

    protected setDisplayedColumns() {
        this.displayedColumns = [].concat(this.DISPLAYED_COLUMNS);
        this.condition.columns.filter(
            column => column.enabled
        ).forEach(
            column => {
                this.displayedColumns.push(column.name);
            }
        );
    }

    protected openSearchSetting(id?: number) {
        let dialogRef = this.dialog.open(
            SearchSettingComponent,
            {width: '100vw', height: '100vh', maxWidth:'100vw', data: {id: id, selectionValues: this.selectionValues}},
        );
        dialogRef.afterClosed().subscribe(
            result => {
                console.log('search setting closed.');
                this.searchSetting.find(this.targetName).subscribe(
                    res => {
                        this.condition = res;
                        this.selectedConditionId = res.id;
                        console.log(this.condition);
                        this.setDisplayedColumns();
                    }
                );
                this.handlePageEvent();
            }
        )
    }

    public isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
      }

    public masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    public showRelations(id: number) {
        console.log(`show relations.${id}`);
        this.service.getRelations(id).subscribe(
            res => {
                let content = this.buildRelatedContent(id, res);
                this.openSheet(content);
            }
        );
    }

    public showAttachments(id: number) {
        console.log(`show attachments.${id}`);
        this.service.getAttachments(id).subscribe(
            res => {
                let content = this.buildAttachmentContent(id, res);
                this.openSheet(content);
            }
        );
    }

    public search() {
        this.service.search(this.condition).subscribe(
            res => {
                console.log(res);
                this.dataSource.data = res.results;
                this.totalCount = res.total;
                if (this.totalCount === 0) {
                    this.showMessage('message.common.search.no_result');
                }
            }
        );
    }

    protected buildRelatedContent(id: number, items: WorkItem[]) {
        let sheetItems: SheetItem[] = [];
        let sheetContent: SheetContent = {
            id: id,
            title: 'screen.integration.label.issue',
            subTitle: 'screen.common.label.related_items',
            items: sheetItems
        };
        items.forEach(
            item => {
                sheetItems.push(
                    {
                        name: item.name,
                        action: null
                    }
                )
            }
        );
        return sheetContent;
    }

    protected buildAttachmentContent(id: number, items: Attachment[]) {
        let sheetItems: SheetItem[] = [];
        let sheetContent: SheetContent = {
            id: id,
            title: 'screen.integration.label.issue',
            subTitle: 'screen.common.label.attachments',
            items: sheetItems
        };
        items.forEach(
            item => {
                sheetItems.push(
                    {
                        name: item.filename,
                        action: item => {console.log(item);},
                        icon: 'get_app'
                    }
                )
            }
        );
        return sheetContent;
    }

    public openSearch() {
        this.searchOpened = !this.searchOpened;
    }

    public importExcel(files: FileList) {
        if (!files || !files.length) {
            return
        }
        let file = files[0];
        console.log(`import excel.${file.name}`);
        if (!file.name.endsWith('.xlsx')) {
            this.showMessage('message.error.common.excel.invalid');
        }
    }

    public clear() {
        this.condition.clear();
    }

    public selectCondition($event) {
        if (!$event.value) {
            return;
        }
        this.searchSetting.get($event.value).subscribe(
            res => {
                this.condition = res;
                this.selectedConditionId = res.id;
                this.setDisplayedColumns();
                this.handlePageEvent();
            }
        );
    }

}
