import { SelectionModel } from '@angular/cdk/collections';
import { ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { Observable } from '../../../../node_modules/rxjs';
import { ConditionModel } from '../condition-model';
import { Model } from '../model';
import { SearchResult } from '../search-result';
import { BizService } from '../services/biz.service';
import { ScreenBehaviorService } from '../services/screen-behavior.service';
import { BaseComponent } from './base.component';

export abstract class BaseDialogComponent<T extends Model, TCondition extends ConditionModel, TComponent, TData> extends BaseComponent {
    
    protected static DISPLAYED_COLUMNS = ['select'];

    public displayedColumns = [];

    public condition: TCondition;

    public isSearchOpen = false;

    public dataSource = new MatTableDataSource<T>();

    public selection = new SelectionModel<T>(true, []);

    protected targetName: string;

    @ViewChild(MatPaginator) paginator: MatPaginator;

    @ViewChild(MatSort) sort: MatSort;

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        protected service: BizService<T>,
        public dialogRef: MatDialogRef<TComponent>,
        public data: TData,
    ) {
        super(router, route, translation, dialog, behavior);
    }

    protected initDataSource() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        // this.searchSetting.find(this.targetName).subscribe(
        //     res => {
        //         this.condition = res;
        //         console.log(this.condition);
        //         this.setDisplayedColumns();
        //         this.search();
        //     }
        // );
    }

    // protected setDisplayedColumns() {
    //     this.displayedColumns = [].concat(this.DISPLAYED_COLUMNS);
    //     this.condition.columns.filter(
    //         column => column.enabled
    //     ).forEach(
    //         column => {
    //             this.displayedColumns.push(column.name);
    //         }
    //     );
    // }

    public isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
      }

    public masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    public showRelated(id: number) {
        console.log('show related. ' + id);
        this.openSheet();
    }

    public search() {
        (this.service.search(this.condition) as Observable<SearchResult<T>>).subscribe(
            res => {
                console.log(res);
                this.dataSource.data = res.results;
                this.paginator.length = res.total;
            }
        );
    }

}
