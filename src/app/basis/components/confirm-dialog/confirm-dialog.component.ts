import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService, Language } from 'angular-l10n';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BaseDialogComponent } from '../base-dialog.component';
import { Model } from '../../model';
import { ScreenBehaviorService } from '../../services/screen-behavior.service';
import { NullService } from '../../services/null.service';
import { ConditionModel } from '../../condition-model';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

    public message: string;

    @Language() lang;

    constructor(
        dialogRef: MatDialogRef<ConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data: boolean | string
    ) {
        this.message = String(data);
    }

    ngOnInit() {
    }

}

export interface CofirmContent {

    message: string;

    result: boolean;

}