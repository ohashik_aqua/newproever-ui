import { OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Language, TranslationService } from 'angular-l10n';
import { ScreenBehaviorService, SheetContent } from '../services/screen-behavior.service';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

export abstract class BaseComponent implements OnInit {

    @Language() lang: string;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        protected translation: TranslationService,
        protected dialog: MatDialog,
        protected behavior: ScreenBehaviorService
    ) { }

    ngOnInit() {
    }

    protected showMessage(message: string, params?: any[]) {
        let translated = this.translation.translate(message);
        this.behavior.showMessage(translated);
    }

    protected navigate(path: string) {
        this.router.navigate([path]);
    }

    protected openSheet(content?: SheetContent) {
        this.behavior.openSheet(content);
    }

    protected showConfirmDialog(message: string, callback: Function) {
        let dialogRef = this.dialog.open(ConfirmDialogComponent, {data: message});
        dialogRef.afterClosed().subscribe(
            res => {
                if (res) {
                    callback(res);
                }
            }
        );
    }

    public goBack() {
        console.log(`go back.${this.behavior.getPreviousUrl()}`);
        this.router.navigateByUrl(this.behavior.getPreviousUrl());
    }

}
