import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { Model } from '../model';
import { BizService } from '../services/biz.service';
import { ScreenBehaviorService } from '../services/screen-behavior.service';
import { BaseComponent } from './base.component';
import { Observable } from 'rxjs';
import { BaseBizService } from '../services/base-biz.service';
import { HumanResource } from '../../models/human-resource';
import { HumanResourceService } from '../../biz/human-resource/human-resource.service';
import { HumanResourceDialogComponent } from '../../biz/human-resource/human-resource-dialog/human-resource-dialog.component';

export abstract class BaseInputComponent<T extends Model> extends BaseComponent {

    protected targetName: string;

    public model: T;

    public humanResources: HumanResource[];

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        protected service: BaseBizService<T>,
        protected humanResourceService: HumanResourceService
    ) {
        super(router, route, translation, dialog, behavior);
    }

    // protected load(id: number): Observable<T> {
    //             console.log(`load.${id}`)
    //             if (!id) {
    //                 return 
    //             }
    //             this.service.get(id).subscribe(
    //                 res => {
    //                     this.model = res;
    //                 }
    //             );
    //         }
    //     )
    // }

    protected save(callback: Function) {
        console.log(`save.`);
        this.service.save(this.model).subscribe(
            res => {
                callback(res);
            }
        );
    }

    public searchHumanResources() {
        this.humanResourceService.search(null).subscribe(
            res => {
                console.log(res);
                this.humanResources = res.results;
            }
        );
    }

    protected selectHumanResource(): Observable<any> {
        this.humanResources = [];
        let dialogRef = this.dialog.open(HumanResourceDialogComponent, {width: '100vw', height: '100vh', maxWidth:'100vw'});
        return dialogRef.afterClosed();
    }

    protected confirmDelete(callback: Function) {
        this.showConfirmDialog('screen.common.note.confirm.delete', callback);
    }

    public clear() {
        this.model.clear();
    }

}
