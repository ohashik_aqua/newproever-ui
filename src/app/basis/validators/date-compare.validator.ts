import { Directive } from '@angular/core';
import { Validator, FormControl, ValidationErrors, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[dateCompareValidator][ngModel]',
    providers: [  
     {  
      provide: NG_VALIDATORS,
      useExisting: DateCompareValidator,
      multi: true
     }
    ]
})
export class DateCompareValidator implements Validator {

    constructor(private src: Date) { }

    public validate(control: FormControl): ValidationErrors {
        if (!this.src) {
            return null;
        }
        if (!control.value) {
            return null;
        }
        if (!(control.value instanceof Date)) {
            return null;
        }
        if (this.src > control.value) {
            return {
                dateCompareValidator: {
                    valid: false
                }
            }
        }
        return null;
    }

}
