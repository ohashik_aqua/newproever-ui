/**
 * APIのレスポンスを表します。
 */
export class ApiResponse<T> {

    public content: T;

    public status: number;

    public message: string;

    public args: any[];

    public total: number;

}
