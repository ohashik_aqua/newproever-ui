import { Model } from "./model";

export interface SearchResult<T extends Model> {

    total: number;

    results: T[];

}