import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ContextService } from '../services/context.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectGuard implements CanActivate {

    private projectId: number;

    constructor(
        private context: ContextService
    ) { }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        console.log(`can activate.`);
        return this.context.getSelectedProject().pipe<boolean>(
            map(
                res => {
                    console.log(`selected project.${res}`);
                    return Boolean(res);
                }
            )
        );
    }
}
