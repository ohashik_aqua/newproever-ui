
import { Model } from "../basis/model";

/**
 * 添付ファイルを表します。
 */
export class Attachment implements Model {

    public static COLUMNS = ['attachment_id','attachment_filename',];

    /**
     * 添付ID
     */
    public id: number;

    /**
     * 添付ファイル名
     */
    public filename: string;

    /**
     * 添付リンク
     */
    public link: string;

    /**
     * 削除
     */
    public deleted: boolean;

    /**
     * ファイル
     */
    public file: File;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): Attachment {
        if (!json) {
            return this;
        }
        this.id = json['attachment_id'];
        this.filename = json['attachment_filename'];
        this.link = json['attachment_link'];
        this.deleted = json['deleted'];
        this.file = json['file'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['attachment_id'] = this.id;
        json['attachment_filename'] = this.filename;
        json['attachment_link'] = this.link;
        json['deleted'] = this.deleted;
        json['file'] = this.file;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.filename = null;
        this.link = null;
        this.deleted = null;
        this.file = null;
    }

}