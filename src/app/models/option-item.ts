
import { Model } from "../basis/model";

/**
 * オプション項目を表します。
 */
export class OptionItem implements Model {

    public static COLUMNS = ['option_item_id','option_item_name','option_item_value',];

    /**
     * オプション項目ID
     */
    public id: number;

    /**
     * オプション項目名
     */
    public name: string;

    /**
     * オプション項目値
     */
    public value: string;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): OptionItem {
        if (!json) {
            return this;
        }
        this.id = json['option_item_id'];
        this.name = json['option_item_name'];
        this.value = json['option_item_value'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['option_item_id'] = this.id;
        json['option_item_name'] = this.name;
        json['option_item_value'] = this.value;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.name = null;
        this.value = null;
    }

}