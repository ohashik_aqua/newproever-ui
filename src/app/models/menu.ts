
import { Model } from "../basis/model";

/**
 * メニューを表します。
 */
export class Menu implements Model {

    public static COLUMNS = ['menu_id','menu_title','menu_link',];

    /**
     * メニューID
     */
    public id: number;

    /**
     * メニュータイトル
     */
    public title: string;

    /**
     * メニューリンク
     */
    public link: string;

    /**
     * 子メニュー
     */
    public children: Menu[];

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): Menu {
        if (!json) {
            return this;
        }
        this.id = json['menu_id'];
        this.title = json['menu_title'];
        this.link = json['menu_link'];
        this.children = [];
        if (json['children']) {
            json['children'].forEach(
                item => {
                    this.children.push(new Menu().fromJson(item));
                }
            );
        }
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['menu_id'] = this.id;
        json['menu_title'] = this.title;
        json['menu_link'] = this.link;
        json['children'] = [];
        if (this.children) {
            this.children.forEach(
                item => {
                    json['children'].push(item.toJson());
                }
            );
        }
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.title = null;
        this.link = null;
        this.children = [];
    }

}