
import { Model } from "../basis/model";

/**
 * 課題緊急度を表します。
 */
export class IssueUrgency implements Model {

    public static COLUMNS = ['issue_urgency_id','issue_urgency_code','issue_urgency_name',];

    /**
     * 課題緊急度ID
     */
    public id: number;

    /**
     * 課題緊急度コード
     */
    public code: string;

    /**
     * 課題緊急度名
     */
    public name: string;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): IssueUrgency {
        if (!json) {
            return this;
        }
        this.id = json['issue_urgency_id'];
        this.code = json['issue_urgency_code'];
        this.name = json['issue_urgency_name'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['issue_urgency_id'] = this.id;
        json['issue_urgency_code'] = this.code;
        json['issue_urgency_name'] = this.name;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.code = null;
        this.name = null;
    }

}