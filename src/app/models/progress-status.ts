
import { Model } from "../basis/model";

/**
 * 担当者進捗状況を表します。
 */
export class ProgressStatus implements Model {

    public static COLUMNS = ['task_id','task_number','task_start_plan_date','task_end_plan_date','task_plan_effort','task_progress_rate','task_progress_status_id','task_actual_effort','work_item_start_implementation_date','work_item_end_implementation_date','work_item_owner_human_resources_id',];

    /**
     * タスクID
     */
    public taskId: number;

    /**
     * タスクスコープID
     */
    public taskScopeId: number;

    /**
     * タスク番号
     */
    public taskNumber: number;

    /**
     * タスクワークアイテムID
     */
    public taskWorkItemId: number;

    /**
     * タスクWBSレベル1
     */
    public taskWbsLevel1: number;

    /**
     * タスクWBSレベル2
     */
    public taskWbsLevel2: number;

    /**
     * タスクWBSレベル3
     */
    public taskWbsLevel3: number;

    /**
     * タスクWBSレベル4
     */
    public taskWbsLevel4: number;

    /**
     * タスクWBSレベル5
     */
    public taskWbsLevel5: number;

    /**
     * タスク開始予定日
     */
    public taskStartPlanDate: Date;

    /**
     * タスク終了予定日
     */
    public taskEndPlanDate: Date;

    /**
     * タスク予定工数
     */
    public taskPlanEffort: number;

    /**
     * タスク進捗率
     */
    public taskProgressRate: number;

    /**
     * タスク進捗ステータスID
     */
    public taskProgressStatusId: number;

    /**
     * タスク実績工数
     */
    public taskActualEffort: number;

    /**
     * ワークアイテム開始実施日
     */
    public workItemStartImplementationDate: Date;

    /**
     * ワークアイテム終了実施日
     */
    public workItemEndImplementationDate: Date;

    /**
     * ワークアイテム担当者ヒューマンリソースID
     */
    public workItemOwnerHumanResourcesId: number;

    /**
     * ワークアイテム種別
     */
    public workItemType: number;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): ProgressStatus {
        if (!json) {
            return this;
        }
        this.taskId = json['task_id'];
        this.taskScopeId = json['task_scope_id'];
        this.taskNumber = json['task_number'];
        this.taskWorkItemId = json['task_work_item_id'];
        this.taskWbsLevel1 = json['task_wbs_level1'];
        this.taskWbsLevel2 = json['task_wbs_level2'];
        this.taskWbsLevel3 = json['task_wbs_level3'];
        this.taskWbsLevel4 = json['task_wbs_level4'];
        this.taskWbsLevel5 = json['task_wbs_level5'];
        this.taskStartPlanDate = json['task_start_plan_date'];
        this.taskEndPlanDate = json['task_end_plan_date'];
        this.taskPlanEffort = json['task_plan_effort'];
        this.taskProgressRate = json['task_progress_rate'];
        this.taskProgressStatusId = json['task_progress_status_id'];
        this.taskActualEffort = json['task_actual_effort'];
        this.workItemStartImplementationDate = json['work_item_start_implementation_date'];
        this.workItemEndImplementationDate = json['work_item_end_implementation_date'];
        this.workItemOwnerHumanResourcesId = json['work_item_owner_human_resources_id'];
        this.workItemType = json['work_item_type'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['task_id'] = this.taskId;
        json['task_scope_id'] = this.taskScopeId;
        json['task_number'] = this.taskNumber;
        json['task_work_item_id'] = this.taskWorkItemId;
        json['task_wbs_level1'] = this.taskWbsLevel1;
        json['task_wbs_level2'] = this.taskWbsLevel2;
        json['task_wbs_level3'] = this.taskWbsLevel3;
        json['task_wbs_level4'] = this.taskWbsLevel4;
        json['task_wbs_level5'] = this.taskWbsLevel5;
        json['task_start_plan_date'] = this.taskStartPlanDate;
        json['task_end_plan_date'] = this.taskEndPlanDate;
        json['task_plan_effort'] = this.taskPlanEffort;
        json['task_progress_rate'] = this.taskProgressRate;
        json['task_progress_status_id'] = this.taskProgressStatusId;
        json['task_actual_effort'] = this.taskActualEffort;
        json['work_item_start_implementation_date'] = this.workItemStartImplementationDate;
        json['work_item_end_implementation_date'] = this.workItemEndImplementationDate;
        json['work_item_owner_human_resources_id'] = this.workItemOwnerHumanResourcesId;
        json['work_item_type'] = this.workItemType;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.taskId = null;
        this.taskScopeId = null;
        this.taskNumber = null;
        this.taskWorkItemId = null;
        this.taskWbsLevel1 = null;
        this.taskWbsLevel2 = null;
        this.taskWbsLevel3 = null;
        this.taskWbsLevel4 = null;
        this.taskWbsLevel5 = null;
        this.taskStartPlanDate = null;
        this.taskEndPlanDate = null;
        this.taskPlanEffort = null;
        this.taskProgressRate = null;
        this.taskProgressStatusId = null;
        this.taskActualEffort = null;
        this.workItemStartImplementationDate = null;
        this.workItemEndImplementationDate = null;
        this.workItemOwnerHumanResourcesId = null;
        this.workItemType = null;
    }

}