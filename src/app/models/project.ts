
import { Model } from "../basis/model";

/**
 * プロジェクトを表します。
 */
export class Project implements Model {

    public static COLUMNS = ['project_id','project_code','project_name',];

    /**
     * プロジェクトID
     */
    public id: number;

    /**
     * プロジェクトコード
     */
    public code: string;

    /**
     * プロジェクト名
     */
    public name: string;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): Project {
        if (!json) {
            return this;
        }
        this.id = json['project_id'];
        this.code = json['project_code'];
        this.name = json['project_name'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['project_id'] = this.id;
        json['project_code'] = this.code;
        json['project_name'] = this.name;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.code = null;
        this.name = null;
    }

}