
import { Model } from "../basis/model";

/**
 * ワークアイテムを表します。
 */
export class WorkItem implements Model {

    public static COLUMNS = ['work_item_id','work_item_name',];

    /**
     * ワークアイテムID
     */
    public id: number;

    /**
     * ワークアイテム名
     */
    public name: string;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): WorkItem {
        if (!json) {
            return this;
        }
        this.id = json['work_item_id'];
        this.name = json['work_item_name'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['work_item_id'] = this.id;
        json['work_item_name'] = this.name;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.name = null;
    }

}