import { NgModule } from '@angular/core';
import { Router, Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: 'integration',
        loadChildren: './integration/integration.module#IntegrationModule'
    },
    {
        path: 'humanresource',
        loadChildren: './human-resource/human-resource.module#HumanResourceModule'
    },
    {
        path: 'scope',
        loadChildren: './scope/scope.module#ScopeModule'
    },
    {
        path: 'schedule',
        loadChildren: './schedule/schedule.module#ScheduleModule'
    },
    {
        path: 'risk',
        loadChildren: './risk/risk.module#RiskModule'
    },
    {
        path: 'stakeholder',
        loadChildren: './stakeholder/stakeholder.module#StakeholderModule'
    },
    {
        path: 'knowledge',
        loadChildren: './knowledge/knowledge.module#KnowledgeModule'
    },
    {
        path: 'mastermaintenance',
        loadChildren: './master-maintenance/master-maintenance.module#MasterMaintenanceModule'
    }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
})
export class BizRoutingModule { }
