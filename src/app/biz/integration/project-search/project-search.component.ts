import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { BaseSearchComponent } from '../../../basis/components/base-search.component';
import { ScreenBehaviorService } from '../../../basis/services/screen-behavior.service';
import { SearchSettingService } from '../../../basis/services/search-setting.service';
import { Project } from '../../../models/project';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-project-search',
  templateUrl: './project-search.component.html',
  styleUrls: ['./project-search.component.scss']
})
export class ProjectSearchComponent extends BaseSearchComponent<Project> {

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        searchSetting: SearchSettingService,
        service: ProjectService) {
            super(router, route, translation, dialog, behavior, searchSetting, service)
        }

    ngOnInit() {
    }

}
