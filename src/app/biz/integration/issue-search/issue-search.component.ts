import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { BaseSearchComponent } from '../../../basis/components/base-search.component';
import { ScreenBehaviorService } from '../../../basis/services/screen-behavior.service';
import { SearchSettingService } from '../../../basis/services/search-setting.service';
import { Issue } from '../../../models/issue';
import { IssueService } from '../issue.service';
import { IssueStatus } from '../../../models/issue-status';

@Component({
    selector: 'app-issue-search',
    templateUrl: './issue-search.component.html',
    styleUrls: ['./issue-search.component.scss']
})
export class IssueSearchComponent extends BaseSearchComponent<Issue> {

    /** 一覧の選択、操作用の列 */
    protected DISPLAYED_COLUMNS = ['select', 'control'];

    public statuses: IssueStatus[];

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        searchSetting: SearchSettingService,
        private issueService: IssueService
    ) {
        super(router, route, translation, dialog, behavior, searchSetting, issueService);
        this.targetName = 'issue';
    }

    ngOnInit() {
        this.initDataSource();
        this.issueService.getStatuses().subscribe(
            res => {
                this.statuses = res;
                this.selectionValues.push({key: 'issue_status', values: res});
            }
        );
    }

    public complete(issue: Issue) {
        console.log('complete.' + issue.id);
        this.doComplete([issue]);
    }

    public completeSelected() {
        console.log('complete selected.');
        this.doComplete(this.selection.selected);
    }

    private doComplete(issues: Issue[]) {
        this.issueService.complete(issues).subscribe(
            res => {
                this.showMessage('message.issue.complete', [issues.length]);
            }
        );
    }

}
