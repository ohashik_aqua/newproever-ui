import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { forkJoin, Observable } from 'rxjs';
import { BaseInputComponent } from '../../../basis/components/base-input.component';
import { ScreenBehaviorService } from '../../../basis/services/screen-behavior.service';
import { HumanResource } from '../../../models/human-resource';
import { Issue } from '../../../models/issue';
import { IssueStatus } from '../../../models/issue-status';
import { IssueUrgency } from '../../../models/issue-urgency';
import { Phase } from '../../../models/phase';
import { HumanResourceService } from '../../human-resource/human-resource.service';
import { IssueService } from '../issue.service';
import { PhaseService } from '../phase.service';
import { Attachment } from '../../../models/attachment';

@Component({
  selector: 'app-issue-input',
  templateUrl: './issue-input.component.html',
  styleUrls: ['./issue-input.component.scss']
})
export class IssueInputComponent extends BaseInputComponent<Issue> {

    public humanResources: HumanResource[];

    public statuses: IssueStatus[];

    public urgencies: IssueUrgency[];

    public phases: Phase[];

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        private issueService: IssueService,
        humanResourceService: HumanResourceService,
        private phaseService: PhaseService
    ) {
        super(router, route, translation, dialog, behavior, issueService, humanResourceService);
    }

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                let id = +params.id;
                console.log(`init.${id}`)
                let getModel: Observable<Issue>;
                if (!id) {
                    getModel = this.issueService.build();
                } else {
                    getModel = this.issueService.get(id);
                }
                forkJoin(
                    getModel,
                    this.issueService.getStatuses(),
                    this.issueService.getUrgencies(),
                    this.phaseService.search(null)
                ).subscribe(
                    res => {
                        this.model = res[0];
                        this.statuses = res[1];
                        this.urgencies = res[2];
                        this.phases = res[3];
                    }
                );
            }
        );
    }

    private convertFile(attachemt: Attachment){
        // if (!attachemt.file) {
        //     return;
        // }
        // let reader = new FileReader();
        // reader.onloadend = (event) => {
        //     let target: FileReader = <FileReader>event.target;
        //     attachemt.content = target.result.split(',')[1];
        //     console.log(`conver file.${target.result.length}`)
        // }
        // reader.readAsDataURL(attachemt.file);
    }

    public save() {
        let files: File[] = [];
        this.model.attachments.forEach(
            attachemt => {
                if (!attachemt.deleted && !attachemt.id) {
                    files.push(attachemt.file);
                    // this.convertFile(attachemt);
                }
            }
        );
        this.issueService.saveWithFiles(this.model, files).subscribe(
            res => {
                this.showMessage('message.info.integration.issue_input.registration', [res.id]);
                this.goBack();
            }
        );
    }

    public selectCorrespondingHumanResource() {
        this.selectHumanResource().subscribe(
            result => {
                console.log(result);
                if (result) {
                     this.model.correspondingPersonHumanResource= result;
                }
            }
        );
    }
    
    public selectRegisterHumanResource() {
        this.selectHumanResource().subscribe(
            result => {
                console.log(result);
                if (result) {
                     this.model.registeredPersonHumanResource= result;
                }
            }
        );
    }

    public selectWatchHumanResource() {
        this.selectHumanResource().subscribe(
            result => {
                console.log(result);
                if (result) {
                     this.model.watcherHumanResources.push(result);
                }
            }
        );
    }

    public deleteWatchHumanResource(index: number) {
        console.log(`delete watch human resource.${index}`);
        this.model.watcherHumanResources[index].deleted = !this.model.watcherHumanResources[index].deleted;
    }

    public handleFiles(files: FileList) {
        if (!files) {
            return;
        }
        for (let i = 0; i < files.length; i++) {
            let file = files[i];
            console.log(`handle file.${file.name} ${file.size}`);
            let attachment = new Attachment();
            this.model.attachments.push(attachment);
            attachment.filename = file.name;
            attachment.file = file;
        }
    }

    public deleteAttachment(index: number) {
        console.log(`delete attachment.${index}`);
        this.model.attachments[index].deleted = !this.model.attachments[index].deleted;
    }

    public delete() {
        this.confirmDelete(
            confirmed => {
                this.service.delete(this.model).subscribe(
                    res => {
                        this.showMessage('message.info.integration.issue_input.delete', [res.id]);
                        this.goBack();
                    }
                )
            }
        );
    }

}
