import { Injectable } from '@angular/core';
import { ApiService } from '../../basis/services/api.service';
import { BaseBizService } from '../../basis/services/base-biz.service';
import { Project } from '../../models/project';
import { Condition } from '../../basis/condition';
import { Observable } from '../../../../node_modules/rxjs';
import { map } from '../../../../node_modules/rxjs/operators';
import { SearchResult } from '../../basis/search-result';

@Injectable({
  providedIn: 'root'
})
export class ProjectService extends BaseBizService<Project> {

    constructor(
        api: ApiService
    ) {
        super(api);
    }

    public search(condition: Condition): Observable<SearchResult<Project>> {
        return null;
    }

    public get(id: number): Observable<Project> {
        return null;
    }

    public save(project: Project): Observable<Project> {
        return null;
    }

    public delete(project: Project): Observable<Project> {
        return null;
    }

    public build(): Observable<Project> {
        return null;
    }

    public getMyProjects(): Observable<Project[]> {
        return this.api.call<any>('get_my_projects', null).pipe(
            map(
                res => {
                    let results: Project[] = [];
                    res.forEach(
                        item => {
                            results.push(new Project().fromJson(item));
                        }
                    );
                    return results;
                }
            )
        );
    }
}
