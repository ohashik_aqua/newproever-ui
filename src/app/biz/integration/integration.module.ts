import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store'
import { SharedModule } from '../../shared.module';
import { IntegrationRoutingModule } from './integration-routing.module';
import { IssueSearchComponent } from './issue-search/issue-search.component';
import { IssueInputComponent } from './issue-input/issue-input.component';
import { HumanResourceModule } from '../human-resource/human-resource.module';
import { HumanResourceDialogComponent } from '../human-resource/human-resource-dialog/human-resource-dialog.component';
import { BasisModule } from '../../basis/basis.module';
import { SearchSettingComponent } from '../../basis/components/search-setting/search-setting.component';
import { ProjectSearchComponent } from './project-search/project-search.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    IntegrationRoutingModule,
    HumanResourceModule,
    BasisModule
  ],
  declarations: [IssueSearchComponent, IssueInputComponent, ProjectSearchComponent],
  entryComponents: [HumanResourceDialogComponent, SearchSettingComponent]
})
export class IntegrationModule { }
