import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Condition } from '../../basis/condition';
import { ApiService } from '../../basis/services/api.service';
import { Issue } from '../../models/issue';
import { IssueStatus } from '../../models/issue-status';
import { IssueUrgency } from '../../models/issue-urgency';
import { BaseBizService } from '../../basis/services/base-biz.service';
import { SearchResult } from '../../basis/search-result';

@Injectable({
    providedIn: 'root'
})
export class IssueService extends BaseBizService<Issue> {

    public name = 'issue';

    constructor(api: ApiService) {
        super(api);
    }

    public search(condition: Condition): Observable<SearchResult<Issue>> {
        return this.api.call<any>('search_issues', condition).pipe<SearchResult<Issue>>(
            map(
                res => {
                    let results: Issue[] = [];
                    res.results.forEach(
                        item => {
                            results.push(new Issue().fromJson(item));
                        }
                    );
                    return {
                        total: res.total,
                        results: results
                    };
                }
            )
        );
    }

    public get(id: number): Observable<Issue> {
        let issue = new Issue();
        issue.id = id;
        return this.api.call<any>('get_issue', issue).pipe<Issue>(
            map(
                res => {
                    return new Issue().fromJson(res);
                }
            )
        );
    }

    public save(issue: Issue): Observable<Issue> {
        return this.api.call<any>('save_issue', issue).pipe<Issue>(
            map(
                res => {
                    let result = new Issue();
                    return result.fromJson(res);
                }
            )
        );
    }

    public saveWithFiles(issue: Issue, files: File[]): Observable<Issue> {
        return this.api.callWithFiles<any>('save_issue', issue, files).pipe<Issue>(
            map(
                res => {
                    let result = new Issue();
                    return result.fromJson(res);
                }
            )
        );
    }

    public delete(issue: Issue): Observable<Issue> {
        return this.api.call<any>('delete_issue', issue).pipe<Issue>(
            map(
                res => {
                    let result = new Issue();
                    return result.fromJson(res);
                }
            )
        );
    }

    public complete(issues: Issue[]): Observable<Issue[]> {
        return this.api.call<any>('complete_issues', issues).pipe<Issue[]>(
            map(
                res => {
                    return [];
                }
            )
        );
    }

    public startWatch(issues: Issue[]): Observable<any> {
        return null;
    }

    public stopWatch(issues: Issue[]): Observable<any> {
        return null;
    }

    public getStatuses(): Observable<IssueStatus[]> {
        return this.api.call<any>('get_issue_statuses', null).pipe<IssueStatus[]>(
            map(
                res => {
                    let results: IssueStatus[] = [];
                    res.forEach(
                        item => {
                            results.push(new IssueStatus().fromJson(item));
                        }
                    );
                    return results;
                }
            )
        );
    }

    public getUrgencies(): Observable<IssueUrgency[]> {
        return this.api.call<any>('get_issue_urgencies', null).pipe<IssueUrgency[]>(
            map(
                res => {
                    let results: IssueUrgency[] = [];
                    res.forEach(
                        item => {
                            results.push(new IssueUrgency().fromJson(item));
                        }
                    );
                    return results;
                }
            )
        );
    }

    public build(): Observable<Issue> {
        return this.api.call<any>('init_issue', null).pipe<Issue>(
            map(
                res => {
                    let result = new Issue();
                    return result.fromJson(res);
                }
            )
        );
    }

}
