import { NgModule } from '@angular/core';
import { Router, Routes, RouterModule } from '@angular/router';
import { IssueSearchComponent } from './issue-search/issue-search.component';
import { IssueInputComponent } from './issue-input/issue-input.component';

const routes: Routes = [
    {
        path: 'issuesearch',
        component: IssueSearchComponent
    },
    {
        path: 'issueinput/:id',
        component: IssueInputComponent
    }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
})
export class IntegrationRoutingModule { }
