import { Injectable } from '@angular/core';
import { BizService } from '../../basis/services/biz.service';
import { Phase } from '../../models/phase';
import { ApiService } from '../../basis/services/api.service';
import { Condition } from '../../basis/condition';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PhaseService implements BizService<Phase> {

    constructor(private api: ApiService) { }

    public search(condition: Condition): Observable<Phase[]> {
        return this.api.call<any[]>('get_phases', condition).pipe<Phase[]>(
            map(
                res => {
                    let results: Phase[] = [];
                    res.forEach(
                        item => {
                            results.push(new Phase().fromJson(item));
                        }
                    );
                    return results;
                }
            )
        );
    }

    public get(id: number): Observable<Phase> {
        return null;
    }

    public save(phase: Phase): Observable<Phase> {
        return null;
    }

    public delete(phase: Phase): Observable<Phase> {
        return null;
    }

    public build(): Observable<Phase> {
        return null;
    }
}
