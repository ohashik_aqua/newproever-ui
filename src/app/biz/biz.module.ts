import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BizRoutingModule } from './biz-routing.module';

@NgModule({
  imports: [
    CommonModule,
    BizRoutingModule
  ],
  declarations: []
})
export class BizModule { }
