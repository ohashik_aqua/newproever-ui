import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HumanResourceDialogComponent } from './human-resource-dialog/human-resource-dialog.component';
import { PartsModule } from '../../parts/parts.module';
import { SharedModule } from '../../shared.module';
import { HumanResourceRoutingModule } from './human-resource-routing.module';
import { HumanResourceSearchComponent } from './human-resource-search/human-resource-search.component';
import { HumanResourceInputComponent } from './human-resource-input/human-resource-input.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PartsModule,
    SharedModule,
    HumanResourceRoutingModule
  ],
  declarations: [HumanResourceDialogComponent, HumanResourceSearchComponent, HumanResourceInputComponent],
  exports: [HumanResourceDialogComponent],
  entryComponents: []
})
export class HumanResourceModule { }
