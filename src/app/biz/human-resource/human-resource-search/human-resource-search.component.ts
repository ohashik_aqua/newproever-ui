import { Component, OnInit } from '@angular/core';
import { BaseSearchComponent } from '../../../basis/components/base-search.component';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { MatDialog } from '@angular/material';
import { ScreenBehaviorService } from '../../../basis/services/screen-behavior.service';
import { SearchSettingService } from '../../../basis/services/search-setting.service';
import { HumanResource } from '../../../models/human-resource';
import { HumanResourceService } from '../human-resource.service';
import { BaseFixedSearchComponent } from '../../../basis/components/base-fixed-search.component';
import { HumanResourceCondition } from '../../../models/human-resource-condition';

@Component({
  selector: 'app-human-resource-search',
  templateUrl: './human-resource-search.component.html',
  styleUrls: ['./human-resource-search.component.scss']
})
export class HumanResourceSearchComponent extends BaseFixedSearchComponent<HumanResource, HumanResourceCondition> {

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        searchSetting: SearchSettingService,
        service: HumanResourceService
    ) {
        super(router, route, translation, dialog, behavior, searchSetting, service);
    }

    ngOnInit() {
        this.condition = new HumanResourceCondition();
        this.displayedColumns = HumanResource.COLUMNS;
        this.initDataSource();
    }

}
