import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { BaseDialogComponent } from '../../../basis/components/base-dialog.component';
import { ScreenBehaviorService } from '../../../basis/services/screen-behavior.service';
import { HumanResource } from '../../../models/human-resource';
import { HumanResourceCondition } from '../../../models/human-resource-condition';
import { HumanResourceService } from '../human-resource.service';

@Component({
  selector: 'app-human-resource-dialog',
  templateUrl: './human-resource-dialog.component.html',
  styleUrls: ['./human-resource-dialog.component.scss']
})
export class HumanResourceDialogComponent extends BaseDialogComponent<HumanResource, HumanResourceCondition, HumanResourceDialogComponent, HumanResource> {

    constructor(
        router: Router,
        route: ActivatedRoute,
        translation: TranslationService,
        dialog: MatDialog,
        behavior: ScreenBehaviorService,
        service: HumanResourceService,
        dialogRef: MatDialogRef<HumanResourceDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data: HumanResource
    ) {
        super(router, route, translation, dialog, behavior, service, dialogRef, data)
    }

    ngOnInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.condition = new HumanResourceCondition();
        this.displayedColumns = BaseDialogComponent.DISPLAYED_COLUMNS.concat(HumanResource.COLUMNS)
        this.search();
    }

    public close() {
        this.dialogRef.close();
    }

}
