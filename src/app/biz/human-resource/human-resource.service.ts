import { Injectable } from '@angular/core';
import { ApiService } from '../../basis/services/api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { HumanResource } from '../../models/human-resource';
import { map } from 'rxjs/operators';
import { Condition } from '../../basis/condition';
import { BizService } from '../../basis/services/biz.service';
import { SearchResult } from '../../basis/search-result';
import { BaseBizService } from '../../basis/services/base-biz.service';
import { Model } from '../../basis/model';

@Injectable({
  providedIn: 'root'
})
export class HumanResourceService extends BaseBizService<HumanResource> {

    constructor(api: ApiService) {
        super(api);
    }

    public search(condition: Model): Observable<SearchResult<HumanResource>> {
        return this.api.call<any>('search_human_resources', condition).pipe<SearchResult<HumanResource>>(
            map(
                res => {
                    let results: HumanResource[] = [];
                    res.results.forEach(
                        element => {
                            results.push(new HumanResource().fromJson(element));
                        }
                    );
                    return {
                        total: res.total,
                        results: results
                    }
                }
            )
        );
    }

    public get(id: number): Observable<HumanResource> {
        let humanResource = new HumanResource();
        humanResource.id = id;
        return this.api.call<any>('get_human_resource', humanResource).pipe<HumanResource>(
            map(
                res => {
                    return new HumanResource().fromJson(res);
                }
            )
        );
    }

    public save(humanResource: HumanResource): Observable<HumanResource> {
        return this.api.call<any>('save_human_resource', humanResource).pipe<HumanResource>(
            map(
                res => {
                    return new HumanResource().fromJson(res);
                }
            )
        );
    }

    public delete(humanResource: HumanResource): Observable<HumanResource> {
        return this.api.call<any>('delete_human_resource', humanResource).pipe<HumanResource>(
            map(
                res => {
                    return new HumanResource().fromJson(res);
                }
            )
        );
    }

    public build(): Observable<HumanResource> {
        let humanResource = new HumanResource();
        return new BehaviorSubject(humanResource).asObservable();
    }
}
