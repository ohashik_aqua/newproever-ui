import { Component, OnInit } from '@angular/core';
import { BaseInputComponent } from '../../../basis/components/base-input.component';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { MatDialog } from '@angular/material';
import { ScreenBehaviorService } from '../../../basis/services/screen-behavior.service';
import { HumanResourceService } from '../human-resource.service';
import { HumanResource } from '../../../models/human-resource';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-human-resource-input',
  templateUrl: './human-resource-input.component.html',
  styleUrls: ['./human-resource-input.component.scss']
})
export class HumanResourceInputComponent extends BaseInputComponent<HumanResource> {

  constructor(
    router: Router,
    route: ActivatedRoute,
    translation: TranslationService,
    dialog: MatDialog,
    behavior: ScreenBehaviorService,
    service: HumanResourceService) {
        super(router, route, translation, dialog, behavior, service, service);
    }

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                let id = +params.id;
                let getModel: Observable<HumanResource>;
                if (!id) {
                    getModel = this.service.build();
                } else {
                    getModel = this.service.get(id);
                }
                getModel.subscribe(
                    res => {
                        this.model = res;
                    }
                );
            }
        );
    }

    public save() {
        this.service.save(this.model).subscribe(
            res => {
                this.showMessage('message.info.human_resource.human_resource_input.registration', [res.id]);
                this.goBack();
            }
        )
    }

    public delete() {
        this.confirmDelete(
            confirmed => {
                this.service.delete(this.model).subscribe(
                    res => {
                        this.showMessage('message.info.human_resource.human_resource_input.delete', [res.id]);
                        this.goBack();
                    }
                )
            }
        );
    }

}
