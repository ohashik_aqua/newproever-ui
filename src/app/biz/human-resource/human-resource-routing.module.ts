import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HumanResourceSearchComponent } from './human-resource-search/human-resource-search.component';
import { HumanResourceInputComponent } from './human-resource-input/human-resource-input.component';

const routes: Routes = [
    {
        path: 'humanresourcesearch',
        component: HumanResourceSearchComponent
    },
    {
        path: 'humanresourceinput/:id',
        component: HumanResourceInputComponent
    }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [],
})
export class HumanResourceRoutingModule { }
