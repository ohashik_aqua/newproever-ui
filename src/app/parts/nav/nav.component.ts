import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Language } from 'angular-l10n';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material';
import { Observable, of as observableOf } from 'rxjs';
import { MenuService } from '../../basis/services/menu.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

    @Language() lang: string;

    public menus = [
        {
            title: 'menu.integration',
            children: [
                {
                    title: 'menu.integration.issue',
                    link: 'biz/integration/issuesearch'
                },
                {
                    title: 'menu.integration.change',
                    link: ''
                }
            ]
        },
        {
            title: 'menu.scope',
            children: [
                {
                    title: 'menu.scope.scope',
                    link: ''
                },
                {
                    title: 'menu.scope.wbs',
                    link: ''
                }
            ]
        }
    ];

    public treeControl: FlatTreeControl<MenuFlatNode>;

    public treeFlattener: MatTreeFlattener<MenuNode, MenuFlatNode>;

    public dataSource: MatTreeFlatDataSource<MenuNode, MenuFlatNode>;

    constructor(
        private router: Router,
        private menuService: MenuService
    ) {
        this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren);
        this.treeControl = new FlatTreeControl<MenuFlatNode>(this.getLevel, this.isExpandable);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
        this.menuService.getMenus().subscribe(
            res => {
                this.dataSource.data = res;
            }
        );
    }

    ngOnInit() {
    }

    public navigate(link: string) {
        console.log('navigate. ' + link);
        this.router.navigate([link]);
    }

    private transformer = (node: MenuNode, level: number) => {
        return new MenuFlatNode(!!node.children && node.children.length > 0, node.title, level, node.link);
    }

    private getLevel = (node: MenuFlatNode) => node.level;

    private isExpandable = (node: MenuFlatNode) => node.expandable;

    private getChildren = (node: MenuNode): Observable<MenuNode[]> => observableOf(node.children);

    public hasChild = (_: number, nodeData: MenuFlatNode) => nodeData.expandable;

}

export interface MenuNode {
    title: string;

    link?: string;

    children?: MenuNode[];
}

export class MenuFlatNode {
    constructor(
        public expandable: boolean,
        public title: string,
        public level: number,
        public link: string
    ) {}
}
