import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Language } from 'angular-l10n';
import { ProjectService } from '../../biz/integration/project.service';
import { ContextService } from '../../basis/services/context.service';
import { Project } from '../../models/project';
import { MatSelectChange, MatDialog } from '../../../../node_modules/@angular/material';
import { ScreenBehaviorService } from '../../basis/services/screen-behavior.service';
import { first } from 'rxjs/operators';
import { ConfirmDialogComponent } from '../../basis/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    @Language() lang: string;

    public projects: Project[];

    public selectedProjectId: number;

    constructor(
        private router: Router,
        private context: ContextService,
        private dialog: MatDialog,
        private projectService: ProjectService,
        private behavior: ScreenBehaviorService
    ) { }

    ngOnInit() {
        console.log(`header init.`);
        this.projectService.getMyProjects().subscribe(
            res => {
                this.projects = res;
                this.context.getSelectedProject().subscribe(
                    project => {
                        console.log(`project.${project}`);
                        this.selectedProjectId = project ? project.id : 0;
                    }
                )
            }
        );
    }

    public goMypage() {
        console.log('go mypage.');
        this.router.navigate(['mypage']);
    }

    public selectProject(event) {
        if (!event.value) {
            this.context.clearSelectedProject();
            return;
        }
        console.log(`select project.${event.value.id}`);
        this.context.setSelectedProject(this.projects.find(
            item => item.id === event.value
        ));
    }

    public logout() {
        this.showConfirmDialog('screen.common.note.confirm.logout',
            res => {
                this.router.navigate(['auth/login']);
            }
        );
    }

    private showConfirmDialog(message: string, callback: Function) {
        let dialogRef = this.dialog.open(ConfirmDialogComponent, {data: message});
        dialogRef.afterClosed().subscribe(
            res => {
                if (res) {
                    callback(res);
                }
            }
        );
    }

}
