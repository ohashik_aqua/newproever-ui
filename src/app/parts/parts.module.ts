import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { DialogHeaderComponent } from './dialog-header/dialog-header.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
      HeaderComponent, FooterComponent, NavComponent, DialogHeaderComponent
  ],
  exports: [
    HeaderComponent, FooterComponent, NavComponent, DialogHeaderComponent
  ]
})
export class PartsModule { }
