import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dialog-header',
  templateUrl: './dialog-header.component.html',
  styleUrls: ['./dialog-header.component.scss']
})
export class DialogHeaderComponent implements OnInit {

    @Output() onClose = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    public close() {
        this.onClose.emit();
    }

}
