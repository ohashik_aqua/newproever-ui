import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { LocaleService, TranslationService, Language } from 'angular-l10n';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {

    @Language() lang: string;

    title: string;

    constructor(public locale: LocaleService, public translation: TranslationService) { }

    ngOnInit(): void {
        // this.translation.translationChanged().subscribe(
        //     () => { this.title = this.translation.translate('Title'); }
        // );
        // this.locale.setCurrentLanguage('ja');
    }

    // selectLanguage(language: string): void {
    //     this.locale.setCurrentLanguage(language);
    // }
}
