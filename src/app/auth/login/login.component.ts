import { Component, OnInit } from '@angular/core';
import { ContextService } from '../../basis/services/context.service';
import { Router } from '@angular/router';
import { Language } from 'angular-l10n';
import { User } from '../../models/user';
import { ScreenBehaviorService } from '../../basis/services/screen-behavior.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public user: User;

    public languageCode: string;

    public languages = [
        {
            code: 'en',
            name: 'English'
        },
        {
            code: 'ja',
            name: '日本語'
        },
        {
            code: 'zh-cn',
            name: '简体中文'
        },
        {
            code: 'zh-tw',
            name: '繁体中文'
        }
    ];

    @Language() lang;

    constructor(
        private router: Router,
        private context: ContextService,
        private behavior: ScreenBehaviorService
    ) { }

    ngOnInit() {
        this.user = new User();
        this.languageCode = this.context.getLanguage();
        this.context.setUserId('');
        this.behavior.init();
    }

    public login() {
        this.context.setUserId(this.user.id);
        this.router.navigate(['top']);
    }

    public changeLanguage(lang: string) {
        this.languageCode = lang;
        this.context.setLanguage(lang);
    }

}
